import { Habilidad } from '../model/habilidad.model';

export const habilidadesMock: Habilidad[] = [
  {
    nombre: 'Angular'
  },
  {
    nombre: 'Java'
  },
  {
    nombre: 'Node.js'
  },
  {
    nombre: 'PHP'
  },
  {
    nombre: 'HTML5'
  },
  {
    nombre: 'Bootstrap'
  },
  {
    nombre: 'Linux'
  }
];
