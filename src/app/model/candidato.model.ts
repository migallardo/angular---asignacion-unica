import { Habilidad } from './habilidad.model';

export class Candidato{

  numero: number;
  habilidades: Habilidad[];

}
