import { Component, OnInit } from '@angular/core';
import { Candidato } from './model/candidato.model';
import { Habilidad } from './model/habilidad.model';
import { habilidadesMock } from './mock/habilidades.mock';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public candidatos: Candidato[] = [];
  public habilidades: Habilidad[] = [];

  public candidatoSeleccionado: Candidato;



  /**
   * Metodo que es invocado al llamar la vista
   */
  ngOnInit(): void {

    this.obtenerCandidatos();

  }


  /**
   * Metodo que obtiene el listado de candidatos.
   * Se simula la peticion a un servicio web que
   * obtiene el listado de candidatos
   */
  obtenerCandidatos() {

    for (let i = 0; i < 12; i++) {
      const candidato: Candidato = new Candidato();
      candidato.numero = i + 1;
      candidato.habilidades = new Array();
      this.candidatos.push(candidato);
    }

  }

  /**
   * Metodo que obtiene el listado de habilidades.
   * Se simula la peticion a un servicio web que
   * obtiene el listado de habilidades
   */
  obtenerHabilidades() {

    this.habilidades = [];
    if (this.candidatoSeleccionado.habilidades.length === 0) {
      Object.assign(this.habilidades, habilidadesMock);
    } else {
      this.filtrarHabilidades();
    }

  }

  /**
   * Metodo encargdo de filtrar las habilidades ya asignadas.
   */
  filtrarHabilidades() {
    habilidadesMock.forEach(habilidad => {
      if (this.candidatoSeleccionado.habilidades.indexOf(habilidad) === -1) {
        this.habilidades.push(habilidad);
      }
    });
  }


  /**
   * Metodo que selecciona un candidato para asignar sus habilidades.
   * @param index Indice del candidato seleccionado.
   */
  seleccionarCandidato(index: number) {
    this.candidatoSeleccionado = new Candidato();
    this.candidatoSeleccionado = this.candidatos[index];
    this.obtenerHabilidades();
  }

  /**
   * Asigna una habilidad a un candidato.
   * @param habilidad Habilidad a asignar al candidato.
   */
  asignarHabilidad(habilidad: Habilidad, index: number) {
    this.candidatoSeleccionado.habilidades.push(habilidad);
    this.habilidades.splice(index, 1);
  }

  quitarHabilidad(habilidad: Habilidad, index: number) {
    this.habilidades.push(habilidad);
    this.candidatoSeleccionado.habilidades.splice(index, 1);
  }

  volver() {
    delete this.candidatoSeleccionado;
  }


}
